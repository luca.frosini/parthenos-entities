package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE22_Persistent_Dataset;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE24_Volatile_Dataset;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP24_has_dataset_snapshot
	<Out extends PE24_Volatile_Dataset, In extends PE22_Persistent_Dataset>
		extends PP17_has_snapshot<Out, In> {

}
