package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;



/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP7i_is_software_object_hosted_by extends
		PP6i_is_digital_object_hosted_by {

}