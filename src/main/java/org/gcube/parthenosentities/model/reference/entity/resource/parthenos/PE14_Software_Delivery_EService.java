package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE14_Software_Delivery_EService extends PE8_EService,
		PE6_Software_Hosting_Service {
	
	//public static final String NAME = "PE14_Software_Delivery_E-Service";
	
}
