package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE22_Persistent_Dataset;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P129_is_about;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP39_is_metadata_for
	<Out extends PE22_Persistent_Dataset, In extends D1_Digital_Object>
		extends P129_is_about<Out, In> {

}
