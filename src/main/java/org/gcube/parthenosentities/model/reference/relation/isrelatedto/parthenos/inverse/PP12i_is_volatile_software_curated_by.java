package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;




/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP12i_is_volatile_software_curated_by extends
		PP11i_is_volatile_digital_object_curated_by {

}
