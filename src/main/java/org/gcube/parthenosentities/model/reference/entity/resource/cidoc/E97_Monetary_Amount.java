package org.gcube.parthenosentities.model.reference.entity.resource.cidoc;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 */
public interface E97_Monetary_Amount extends E54_Dimension {
	
}
