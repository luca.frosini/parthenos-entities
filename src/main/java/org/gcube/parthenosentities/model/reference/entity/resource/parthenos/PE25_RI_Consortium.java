package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E40_Legal_Body;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE25_RI_Consortium extends E40_Legal_Body {

}
