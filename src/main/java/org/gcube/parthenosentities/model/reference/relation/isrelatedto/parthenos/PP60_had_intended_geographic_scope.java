package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E53_Place;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP60_had_intended_geographic_scope
	<Out extends E7_Activity, In extends E53_Place> 
		extends IsRelatedTo<Out, In>  {

}
