package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;




/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP11i_is_volatile_digital_object_curated_by extends
		PP32i_is_curated_by {

}
