package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E39_Actor;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE41_Award_Activity;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P14_carried_out_by;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP53_had_awarder
	<Out extends PE41_Award_Activity, In extends E39_Actor> 
		extends P14_carried_out_by<Out, In>  {

}
