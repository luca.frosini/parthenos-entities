package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E97_Monetary_Amount;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE42_Funding_Activity;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP57_provided_funding_amount
	<Out extends PE42_Funding_Activity, In extends E97_Monetary_Amount> 
		extends IsRelatedTo<Out, In>  {

}
