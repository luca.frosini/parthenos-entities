package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE21_Persistent_Software;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP19_has_persistent_software_part
	<Out extends PE21_Persistent_Software, In extends PE21_Persistent_Software>
		extends PP16_has_persistent_digital_object_part<Out, In> {

}
