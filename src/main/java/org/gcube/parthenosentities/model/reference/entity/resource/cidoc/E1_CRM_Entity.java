package org.gcube.parthenosentities.model.reference.entity.resource.cidoc;

import org.gcube.informationsystem.model.reference.entities.Resource;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Scope note: This class comprises all things in the universe of discourse of the CIDOC Conceptual Reference Model.
 * 
 * It is an abstract concept providing for three general properties:
 * 1. Identification by name or appellation, and in particular by a preferred identifier
 * 2. Classification by type, allowing further refinement of the specific subclass an instance belongs to
 * 3. Attachment of free text for the expression of anything not captured by formal properties
 * 
 * With the exception of E59 Primitive Value, all other classes within the CRM are directly or indirectly 
 * specialisations of E1 CRM Entity.
 * 
 * Examples:
 * - the earthquake in Lisbon 1755 (E5)
 * 
 * In First Order Logic:
 * E1(x)
 * 
 * Properties:
 * P1 is identified by (identifies): E41 Appellation
 * P2 has type (is type of): E55 Type
 * P3 has note: E62 String
 * 			(P3.1 has type: E55 Type)
 * P48 has preferred identifier (is preferred identifier of): E42 Identifier
 * P137 exemplifies (is exemplified by): E55 Type
 * 			(P137.1 in the taxonomic role: E55 Type)
 */
public interface E1_CRM_Entity extends Resource {
	
}
