package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.resourcemanagement.model.reference.entities.resources.EService;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE8_EService extends PE1_Service, EService {

	//public static final String NAME = "PE8_E-Service";
	
}
