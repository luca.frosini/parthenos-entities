package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;

import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.inverse.P14i_performed;



/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP2i_provides extends P14i_performed {

}
