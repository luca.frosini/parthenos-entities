package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE34_Team;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE35_Project;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P17_was_motivated_by;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP44_has_maintaining_team<Out extends PE35_Project, In extends PE34_Team> 
	extends P17_was_motivated_by<Out, In> {

}
