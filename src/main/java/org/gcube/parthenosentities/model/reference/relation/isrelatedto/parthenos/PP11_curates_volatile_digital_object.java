package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE10_Digital_Curating_Service;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE20_Volatile_Digital_Object;


public interface PP11_curates_volatile_digital_object
	<Out extends PE10_Digital_Curating_Service, In extends PE20_Volatile_Digital_Object> 
		extends PP32_curates<Out,In> {

}
