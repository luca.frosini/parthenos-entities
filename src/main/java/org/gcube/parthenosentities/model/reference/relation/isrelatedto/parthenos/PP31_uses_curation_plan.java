package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE28_Curation_Plan;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE3_Curating_Service;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P33_used_specific_technique;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP31_uses_curation_plan
	<Out extends PE3_Curating_Service, In extends PE28_Curation_Plan> 
		extends P33_used_specific_technique<Out, In> {

}