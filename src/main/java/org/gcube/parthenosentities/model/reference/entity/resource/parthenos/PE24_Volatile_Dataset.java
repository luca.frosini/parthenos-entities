package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE24_Volatile_Dataset extends PE18_Dataset,
		PE20_Volatile_Digital_Object {

}
