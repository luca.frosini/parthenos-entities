package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;
import org.gcube.resourcemanagement.model.reference.entities.resources.Dataset;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE18_Dataset extends Dataset, D1_Digital_Object {

}
