package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E55_Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE39_Availability_Type extends E55_Type {

}
