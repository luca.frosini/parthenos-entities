package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;

/**
 * @author Luca Frosini (ISTI - CNR)
 * P129 is about (is subject of)
 * 
 * Domain: E89 Propositional Object
 * Range: E1 CRM Entity
 * Subproperty: E89 Propositional Object. P67 refers to (is referred to by): 
 * E1 CRM Entity
 * Quantification: many to many (0,n:0,n)
 *
 * Scope note: This property documents that an E89 Propositional Object has as 
 * subject an instance of E1 CRM Entity.
 * This differs from P67 refers to (is referred to by), which refers to an E1 
 * CRM Entity, in that it describes the primary subject or subjects of an E89 
 * Propositional Object.
 * 
 * Examples:
 *  The text entitled ‘Reach for the sky’ (E33) is about Douglas Bader (E21)
 * 
 * In First Order Logic:
 * P129(x,y) ⊃ E89(x)
 * 
 * Definition of the CIDOC Conceptual Reference Model version 6.2.2
 * 90P129(x,y) ⊃ E1(y)
 * P129(x,y) ⊃ P67(x,y)
 * 
 */
public interface P129_is_about<Out extends Resource, In extends Resource>
		extends IsRelatedTo<Out, In> {

}
