package org.gcube.parthenosentities.model.reference.relation.consistsof.parthenos.inverse;

import org.gcube.parthenosentities.model.reference.relation.consistsof.cidoc.inverse.P1i_identifies;



/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP28i_is_designated_access_point_of extends P1i_identifies {

}
