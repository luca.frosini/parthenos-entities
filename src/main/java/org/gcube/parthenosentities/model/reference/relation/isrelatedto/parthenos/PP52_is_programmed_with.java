package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D14_Software;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE40_Programing_Language;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP52_is_programmed_with
	<Out extends D14_Software, In extends PE40_Programing_Language> 
		extends IsRelatedTo<Out, In>  {

}
