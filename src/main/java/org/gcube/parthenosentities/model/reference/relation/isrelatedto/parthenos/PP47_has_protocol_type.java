package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE37_Protocol_Type;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE8_EService;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P125_used_object_of_type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP47_has_protocol_type
	<Out extends PE8_EService, In extends PE37_Protocol_Type> 
		extends P125_used_object_of_type<Out, In> {

}
