package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE18_Dataset;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE24_Volatile_Dataset;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP23_has_dataset_part<Out extends PE24_Volatile_Dataset, In extends PE18_Dataset>
		extends PP18_has_digital_object_part<Out, In> {

}
