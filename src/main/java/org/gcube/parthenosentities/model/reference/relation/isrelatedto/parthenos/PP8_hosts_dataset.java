package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE18_Dataset;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE7_Data_Hosting_Service;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Manages;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP8_hosts_dataset
	<Out extends PE7_Data_Hosting_Service, In extends PE18_Dataset> 
		extends PP6_hosts_digital_object<Out,In>, Manages<Out, In> {

}
