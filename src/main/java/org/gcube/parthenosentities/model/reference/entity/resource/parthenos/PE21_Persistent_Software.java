package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D14_Software;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE21_Persistent_Software extends D14_Software,
		PE19_Persistent_Digital_Object {

}
