package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D14_Software;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE23_Volatile_Software;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP21_has_software_part
	<Out extends PE23_Volatile_Software, In extends D14_Software> 
		extends PP18_has_digital_object_part<Out, In> {

}
