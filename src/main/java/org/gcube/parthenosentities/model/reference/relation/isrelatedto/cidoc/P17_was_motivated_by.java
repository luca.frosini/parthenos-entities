package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * Domain: E7 Activity
 * Range: E1 CRM Entity
 * 
 * Subproperty of: E7 Activity. P15 was influenced by (influenced): E1 CRM Entity
 * 
 * Quantification: many to many (0,n:0,n)
 * 
 * Scope note:
 * This property describes an item or items that are regarded as a reason 
 * for carrying out the E7 Activity.
 * 
 * For example, the discovery of a large hoard of treasure may call for a 
 * celebration, an order from head quarters can start a military manoeuvre.
 * 
 * Examples:
 * the resignation of the chief executive (E7) was motivated by the collapse of SwissAir (E68).
 * the coronation of Elizabeth II (E7) was motivated by the death of George VI (E69)
 * 
 * In First Order Logic:
 * P17(x,y) ⊃ E7(x)
 * P17(x,y) ⊃ E1(y)
 * P17 (x,y) ⊃ P15(x,y)
 * 
 */
public interface P17_was_motivated_by
	<Out extends E7_Activity, In extends Resource> extends IsRelatedTo<Out, In> {

}
