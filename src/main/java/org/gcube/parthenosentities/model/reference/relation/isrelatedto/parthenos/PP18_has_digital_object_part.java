package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE20_Volatile_Digital_Object;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P106_is_composed_of;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP18_has_digital_object_part<Out extends PE20_Volatile_Digital_Object, In extends D1_Digital_Object>
		extends P106_is_composed_of<Out, In> {

}
