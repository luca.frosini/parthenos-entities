package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE20_Volatile_Digital_Object extends PE32_Curated_Thing,
		D1_Digital_Object {

}
