package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;



/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP23i_is_dataset_part_of extends
		PP18i_is_digital_object_part_of {

}
