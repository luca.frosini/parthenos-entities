package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE21_Persistent_Software;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE23_Volatile_Software;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP22_has_release
	<Out extends PE23_Volatile_Software, In extends PE21_Persistent_Software> 
		extends PP17_has_snapshot<Out, In> {

}
