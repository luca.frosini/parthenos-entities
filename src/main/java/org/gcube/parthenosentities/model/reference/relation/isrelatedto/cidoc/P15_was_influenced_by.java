package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * Domain: E7 Activity
 * Range: E1 CRM Entity
 * 
 * Superproperty of: E7 Activity. P16 used specific object (was used for): 
 * E70 Thing E7 Activity. P17 was motivated by (motivated): E1 CRM Entity
 * E7 Activity. P134 continued (was continued by): E7 Activity
 * E83 Type Creation. P136 was based on (supported type creation): E1 CRM Entity
 * 
 * Quantification: many to many (0,n:0,n)
 * 
 * Scope note:
 * This is a high level property, which captures the relationship between an 
 * E7 Activity and anything that may have had some bearing upon it.
 * The property has more specific sub properties.
 * 
 * Examples:
 * the designing of the Sydney Harbour Bridge (E7) was influenced by the Tyne bridge (E22)
 * 
 * In First Order Logic:
 * P15 (x,y) ⊃ E7(x)
 * P15 (x,y) ⊃ E1(y)
 * 
 */
public interface P15_was_influenced_by<Out extends E7_Activity, In extends Resource>
		extends IsRelatedTo<Out, In> {

}
