package org.gcube.parthenosentities.model.reference.entity.facet.parthenos;

import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;

/**
 * @author Luca Frosini (ISTI - CNR)
 * @author Alessia Bardi
 */
public interface E30_Right extends Facet {

	@ISProperty
	public String getRights();

	public void setRights(String rights);
	
}
