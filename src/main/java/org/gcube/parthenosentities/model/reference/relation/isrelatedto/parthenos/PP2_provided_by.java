package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E39_Actor;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE1_Service;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P14_carried_out_by;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP2_provided_by<Out extends PE1_Service, In extends E39_Actor>
		extends P14_carried_out_by<Out, In> {

}
