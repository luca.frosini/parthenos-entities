package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE38_Schema;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE8_EService;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P16_used_specific_object;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP48_uses_protocol_parameter
	<Out extends PE8_EService, In extends PE38_Schema>
		extends P16_used_specific_object<Out, In> {

}
