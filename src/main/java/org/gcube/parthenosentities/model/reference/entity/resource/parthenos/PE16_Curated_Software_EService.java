package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE16_Curated_Software_EService extends
		PE11_Software_Curating_Service, PE14_Software_Delivery_EService,
		PE13_Software_Computing_EService {

	//public static final String NAME = "PE16_Curated_Software_E-Service";
	
}
