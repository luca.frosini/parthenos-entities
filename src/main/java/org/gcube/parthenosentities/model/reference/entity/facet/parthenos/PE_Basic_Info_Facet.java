package org.gcube.parthenosentities.model.reference.entity.facet.parthenos;

import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE_Basic_Info_Facet extends Facet {

	public static final String DESCRIPTION = "This facet is expected to capture title and description metadata for Partenos Entities";
	public static final String VERSION = "1.0.0";
	
	@ISProperty
	public String getTitle();
	
	public void setTitle(String title);
	
	@ISProperty
	public String getDescription();
	
	public void setDescription(String description);
	
}
