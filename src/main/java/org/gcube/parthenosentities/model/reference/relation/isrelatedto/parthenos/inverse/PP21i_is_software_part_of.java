package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;



/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP21i_is_software_part_of extends
		PP18i_is_digital_object_part_of {

}
