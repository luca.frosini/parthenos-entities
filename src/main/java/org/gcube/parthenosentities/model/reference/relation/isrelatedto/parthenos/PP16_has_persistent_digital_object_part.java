package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE19_Persistent_Digital_Object;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P106_is_composed_of;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP16_has_persistent_digital_object_part
		<Out extends PE19_Persistent_Digital_Object, In extends PE19_Persistent_Digital_Object>
		extends P106_is_composed_of<Out, In> {

}
