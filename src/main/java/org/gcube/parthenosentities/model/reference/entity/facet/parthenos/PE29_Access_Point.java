package org.gcube.parthenosentities.model.reference.entity.facet.parthenos;

import org.gcube.parthenosentities.model.reference.entity.facet.cidoc.E51_Contact_Point;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;

/**
 * @author Luca Frosini (ISTI - CNR) 
 * This class comprises instances of web addresses and network addresses by 
 * which e-services can be accessed.
 */
public interface PE29_Access_Point extends E51_Contact_Point, AccessPointFacet {

}
