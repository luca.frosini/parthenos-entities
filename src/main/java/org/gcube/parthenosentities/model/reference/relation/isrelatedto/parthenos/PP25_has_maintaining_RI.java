package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE25_RI_Consortium;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE26_RI_Project;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P15_was_influenced_by;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP25_has_maintaining_RI
	<Out extends PE26_RI_Project, In extends PE25_RI_Consortium>
		extends P15_was_influenced_by<Out, In> {

}
