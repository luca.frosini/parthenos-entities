package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE43_Encoding_Type;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P2_has_type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP58_is_encoded_with
	<Out extends D1_Digital_Object, In extends PE43_Encoding_Type> 
		extends P2_has_type<Out, In>  {

}
