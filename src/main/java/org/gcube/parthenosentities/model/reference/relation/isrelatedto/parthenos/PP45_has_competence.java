package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE1_Service;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE36_Competency_Type;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P21_had_general_purpose;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP45_has_competence
	<Out extends PE1_Service, In extends PE36_Competency_Type> 
		extends P21_had_general_purpose<Out, In>  {

}
