package org.gcube.parthenosentities.model.reference.entity.resource.cidoc;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * This class comprises items of a material nature that are units for documentation and have physical
 * boundaries that separate them completely in an objective way from other objects.
 * The class also includes all aggregates of objects made for functional purposes of whatever kind,
 * independent of physical coherence, such as a set of chessmen. Typically, instances of E19 Physical
 * Object can be moved (if not too heavy).
 * 
 * In some contexts, such objects, except for aggregates, are also called “bona fide objects” (Smith &
 * Varzi, 2000, pp.401-420), i.e. naturally defined objects.
 * 
 * The decision as to what is documented as a complete item, rather than by its parts or components, may
 * be a purely administrative decision or may be a result of the order in which the item was acquired.
 * 
 * Examples:
 * - John Smith
 * - Aphrodite of Milos
 * - the Palace of Knossos
 * - the Cullinan Diamond
 * - Apollo 13 at the time of launch
 * 
 * In First Order Logic:
 * E19(x) ⊃ E18(x)
 * 
 * Properties:
 * P54 has current permanent location (is current permanent location of): E53 Place
 * P55 has current location (currently holds): E53 Place
 * P56 bears feature (is found on): E26 Physical Feature
 * P57 has number of parts: E60 Number
 * 
 */
public interface E19_Physical_Object extends E70_Thing {

}
