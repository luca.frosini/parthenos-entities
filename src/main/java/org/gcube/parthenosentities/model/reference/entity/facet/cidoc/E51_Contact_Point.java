package org.gcube.parthenosentities.model.reference.entity.facet.cidoc;

import org.gcube.informationsystem.model.reference.entities.Facet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface E51_Contact_Point extends Facet {

}
