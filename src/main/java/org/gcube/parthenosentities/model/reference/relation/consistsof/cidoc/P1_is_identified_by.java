package org.gcube.parthenosentities.model.reference.relation.consistsof.cidoc;

import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface P1_is_identified_by<Out extends Resource, In extends Facet>
		extends IsIdentifiedBy<Out, In> {

}
