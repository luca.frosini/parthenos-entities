package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE6_Software_Hosting_Service extends PE5_Digital_Hosting_Service, HostingNode {
	
}
