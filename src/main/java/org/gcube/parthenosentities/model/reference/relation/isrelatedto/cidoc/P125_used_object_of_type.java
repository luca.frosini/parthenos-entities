package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E55_Type;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * Domain: E7 Activity
 * Range: E55 Type
 * 
 * Superproperty of: E7 Activity.
 * P32 used general technique (was technique of): E55 Type
 * 
 * Quantification: many to many (0,n:0,n)
 * 
 * Scope note:
 * This property defines the kind of objects used in an E7 Activity, 
 * when the specific instance is either
 * unknown or not of interest, such as use of "a hammer".
 * 
 * Examples:
 * Aat the Battle of Agincourt (E7),
 * the English archers used object of type long bow (E55)
 * 
 * In First Order Logic:
 * P125(x,y) ⊃ E7(x)
 * P125(x,y) ⊃ E55(y)
 * 
 */
public interface P125_used_object_of_type
	<Out extends E7_Activity, In extends E55_Type> 
		extends IsRelatedTo<Out, In> {

}
