package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E29_Design_or_Procedure;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE28_Curation_Plan extends E29_Design_or_Procedure {

}
