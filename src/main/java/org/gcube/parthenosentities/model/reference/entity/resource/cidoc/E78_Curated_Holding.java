package org.gcube.parthenosentities.model.reference.entity.resource.cidoc;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE32_Curated_Thing;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Scope note: This class comprises aggregations of instances of E18 Physical Thing that are
 * assembled and maintained (“curated” and “preserved,” in museological terminology) by one or more
 * instances of E39 Actor over time for a specific purpose and audience, and according to a particular
 * collection development plan. Typical instances of curated holdings are museum collections, archives,
 * library holdings and digital libraries. A digital library is regarded as an instance of E18 Physical Thing
 * because it requires keeping physical carriers of the electronic content.
 * 
 * Items may be added or removed from an E78 Curated Holding in pursuit of this plan. This class should
 * not be confused with the E39 Actor maintaining the E78 Curated Holding often referred to with the
 * name of the E78 Curated Holding (e.g. “The Wallace Collection decided...”).
 * 
 * Collective objects in the general sense, like a tomb full of gifts, a folder with stamps or a set of
 * chessmen, should be documented as instances of E19 Physical Object, and not as instances of E78
 * Curated Holding. This is because they form wholes either because they are physically bound together
 * or because they are kept together for their functionality.
 * 
 * 
 * Examples:
 * - the John Clayton Herbarium
 * - the Wallace Collection
 * - Mikael Heggelund Foslie’s coralline red algae Herbarium at Museum of Natural History and Archaeology, 
 * 		Trondheim, Norway
 * 
 * In First Order Logic:
 * E78(x) ⊃ E24(x)
 * 
 * Properties:
 * P109 has current or former curator (is current or former curator of): E39 Actor
 */
public interface E78_Curated_Holding extends PE32_Curated_Thing {

}
