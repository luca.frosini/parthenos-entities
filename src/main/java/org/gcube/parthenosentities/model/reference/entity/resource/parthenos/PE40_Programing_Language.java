package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E55_Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE40_Programing_Language extends E55_Type {

}
