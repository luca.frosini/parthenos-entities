package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D14_Software;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE14_Software_Delivery_EService;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P16_used_specific_object;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP15_delivers_on_request
	<Out extends PE14_Software_Delivery_EService, In extends D14_Software>
		extends P16_used_specific_object<Out, In> {

}
