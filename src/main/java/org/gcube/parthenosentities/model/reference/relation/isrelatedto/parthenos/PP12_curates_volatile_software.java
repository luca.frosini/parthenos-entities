package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE11_Software_Curating_Service;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE23_Volatile_Software;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP12_curates_volatile_software
	<Out extends PE11_Software_Curating_Service, In extends PE23_Volatile_Software> 
		extends PP11_curates_volatile_digital_object<Out,In> {

}
