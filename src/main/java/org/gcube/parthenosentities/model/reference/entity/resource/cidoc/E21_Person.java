package org.gcube.parthenosentities.model.reference.entity.resource.cidoc;

import org.gcube.resourcemanagement.model.reference.entities.resources.Person;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface E21_Person extends E39_Actor, Person {
	
}
