package org.gcube.parthenosentities.model.reference.relation.consistsof.parthenos;

import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE1_Service;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP42_has_declarative_time
	<Out extends PE1_Service, In extends EventFacet>
		extends ConsistsOf<Out, In> {

}
