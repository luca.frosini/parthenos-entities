package org.gcube.parthenosentities.model.reference.entity.resource.cidoc;

import org.gcube.parthenosentities.model.reference.entity.facet.parthenos.PE_Contact_Reference_Facet;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Attached facets are:
 * 	- {@link IdentifierFacet} to map ID
 *  - {@link PE_Contact_Reference_Facet}
 *  
 */
public interface E39_Actor extends Actor, E77_Persistent_Item {
	
	
}
