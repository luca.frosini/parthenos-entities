package org.gcube.parthenosentities.model.reference.entity.resource.cidoc;

import org.gcube.resourcemanagement.model.reference.entities.resources.LegalBody;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface E40_Legal_Body extends E74_Group, LegalBody {

}
