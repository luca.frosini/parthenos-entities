package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E1_CRM_Entity;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E55_Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface P2_has_type<Out extends E1_CRM_Entity, In extends E55_Type>
		extends IsRelatedTo<Out, In> {

}
