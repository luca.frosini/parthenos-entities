package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E65_Creation;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE22_Persistent_Dataset;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P16_used_specific_object;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP40_created_successor_of
	<Out extends E65_Creation, In extends PE22_Persistent_Dataset> 
		extends P16_used_specific_object<Out,In> {

}
