package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;

import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.inverse.P106i_forms_part_of;



/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP16i_is_persistent_digital_object_part_of extends
		P106i_forms_part_of {

}
