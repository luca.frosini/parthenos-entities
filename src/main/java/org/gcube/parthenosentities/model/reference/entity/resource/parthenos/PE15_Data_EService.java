package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE15_Data_EService extends PE8_EService,
		PE7_Data_Hosting_Service {
	
	//public static final String NAME = "PE15_Data_E-Service";

}
