package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E1_CRM_Entity;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE41_Award_Activity;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P17_was_motivated_by;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP56_awarded_for
	<Out extends PE41_Award_Activity, In extends E1_CRM_Entity> 
		extends P17_was_motivated_by<Out, In>  {

}
