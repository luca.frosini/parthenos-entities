package org.gcube.parthenosentities.model.reference.entity.resource.cidoc;

import org.gcube.resourcemanagement.model.reference.entities.resources.Software;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface D14_Software extends D1_Digital_Object, Software {

}
