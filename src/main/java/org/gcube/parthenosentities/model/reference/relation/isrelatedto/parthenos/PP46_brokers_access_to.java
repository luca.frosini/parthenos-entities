package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE33_EAccess_Brokering_Service;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE8_EService;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP46_brokers_access_to
	<Out extends PE33_EAccess_Brokering_Service, In extends PE8_EService> 
		extends IsRelatedTo<Out, In> {

}
