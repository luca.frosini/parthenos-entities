package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE1_Service;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE39_Availability_Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP51_has_availability
	<Out extends PE1_Service, In extends PE39_Availability_Type> 
		extends IsRelatedTo<Out, In>  {

}
