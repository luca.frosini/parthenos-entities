package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D14_Software;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE38_Schema extends D14_Software {

}
