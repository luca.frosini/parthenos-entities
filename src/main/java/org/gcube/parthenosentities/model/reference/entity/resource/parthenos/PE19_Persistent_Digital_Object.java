package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE19_Persistent_Digital_Object extends D1_Digital_Object {

}
