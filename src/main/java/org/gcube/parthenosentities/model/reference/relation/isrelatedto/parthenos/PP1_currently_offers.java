package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE1_Service;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE26_RI_Project;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P9_consists_of;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP1_currently_offers
	<Out extends PE26_RI_Project, In extends PE1_Service>
		extends P9_consists_of<Out,In> {

}
