package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;

import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.inverse.P33i_was_used_by;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP31i_is_curation_plan_used_by extends P33i_was_used_by {

}
