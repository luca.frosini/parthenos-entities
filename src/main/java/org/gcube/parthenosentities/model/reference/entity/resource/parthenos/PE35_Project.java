package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE35_Project extends E7_Activity {

}
