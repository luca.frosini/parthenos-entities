package org.gcube.parthenosentities.model.reference.relation.consistsof.parthenos;

import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.parthenosentities.model.reference.entity.facet.parthenos.PE29_Access_Point;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE8_EService;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP49_provides_access_point
	<Out extends PE8_EService, In extends PE29_Access_Point>
		extends ConsistsOf<Out, In> {

}
