package org.gcube.parthenosentities.model.reference.entity.facet.parthenos;

import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactReferenceFacet;

/**
 * @author Luca Frosini (ISTI - CNR)
 * @author Alessia Bardi (ISTI - CNR)
 *
 */
public interface PE_Contact_Reference_Facet extends ContactReferenceFacet {

	public static final String DESCRIPTION = "This facet is expected to capture minimal metadata for E39_Actor";
	public static final String VERSION = "1.0.0";
	
	@ISProperty
	public String getAppellation();
	
	public void setAppellation(String appelation);
	
	@ISProperty
	public String getDescription();
	
	public void setDescription(String description);
	
	@ISProperty
	public String getLegalAddress();
	
	public void setLegalAddress(String legalAddress);
	
	/*
	 * see address
	@ISProperty
	public String getMailingAddress();
	
	public void setMailingAddress(String mailingAddress);
	*/
	
	@ISProperty(regexpr=ContactFacet.EMAIL_PATTERN)
	public String getEMail();

	public void setEMail(String eMail);
	
	
	
}
