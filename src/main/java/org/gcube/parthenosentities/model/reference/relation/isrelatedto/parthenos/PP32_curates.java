package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE32_Curated_Thing;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE3_Curating_Service;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP32_curates
	<Out extends PE3_Curating_Service, In extends PE32_Curated_Thing>
		extends IsRelatedTo<Out, In> {

}
