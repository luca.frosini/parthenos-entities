package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E70_Thing;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;

/**
 * @author Luca Frosini (ISTI - CNR)
 * P16 used specific object (was used for)
 * Domain: E7 Activity
 * Range: E70 Thing
 * Subproperty of: E5 Event. P12 occurred in the presence of (was present at): 
 * E77 Persistent Item
 * 
 * Superproperty of: E7 Activity. P15 was influenced by (influenced): E1 CRM 
 * Entity
 * E7 Activity.P33 used specific technique (was used by):E29 Design or Procedure
 * E15 Identifier Assignment. P142 used constituent (was used in): E90 Symbolic 
 * Object
 * E79 Part Addition. P111 added (was added by):E18 Physical Thing
 * Quantification: many to many (0,n:0,n)
 * 
 * Scope note: This property describes the use of material or immaterial things 
 * in a way essential to the performance or the outcome of an E7 Activity.
 * This property typically applies to tools, instruments, moulds, raw materials 
 * and items embedded in a product. It implies that the presence of the object 
 * in question was a necessary condition for the action.
 * For example, the activity of writing this text required the use of a 
 * computer. An immaterial thing can be used if at least one of its carriers is 
 * present. For example, the software tools on a computer.
 * Another example is the use of a particular name by a particular group of 
 * people over some span to identify a thing, such as a settlement. In this 
 * case, the physical carriers of this name are at least the people 
 * understanding its use.
 * 
 * Examples:
 *   the writing of this scope note (E7) used specific object Nicholas Crofts’
 * computer (E22) mode of use Typing Tool; Storage Medium (E55)
 *   the people of Iraq calling the place identified by TGN ‘7017998’ (E7) used
 * specific object “Quyunjig” (E44) mode of use Current; Vernacular (E55)
 * 
 *  In First Order Logic:
 * P16 (x,y) ⊃ E7(x)
 * P16 (x,y) ⊃ E70(y)
 * P16 (x,y) ⊃ P12(x,y)
 * P16 (x,y) ⊃ P15(x,y)
 * P16(x,y,z) ⊃ [P16(x,y) ∧ E55(z)]
 * 
 * Properties: P16.1 mode of use: E55 Type
 */
public interface P16_used_specific_object
	<Out extends E7_Activity, In extends E70_Thing>
		extends IsRelatedTo<Out, In> {

}
