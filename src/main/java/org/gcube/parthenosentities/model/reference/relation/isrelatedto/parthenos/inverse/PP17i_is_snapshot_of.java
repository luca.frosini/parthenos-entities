package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;

import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.inverse.P130i_features_are_also_found_on;



/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP17i_is_snapshot_of extends P130i_features_are_also_found_on {

}
