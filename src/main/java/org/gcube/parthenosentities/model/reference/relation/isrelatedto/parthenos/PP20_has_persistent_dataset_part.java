package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE22_Persistent_Dataset;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP20_has_persistent_dataset_part
	<Out extends PE22_Persistent_Dataset, In extends PE22_Persistent_Dataset>
		extends PP16_has_persistent_digital_object_part<Out, In> {

}
