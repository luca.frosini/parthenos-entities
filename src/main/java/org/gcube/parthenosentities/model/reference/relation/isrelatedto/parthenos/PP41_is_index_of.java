package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE24_Volatile_Dataset;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP41_is_index_of
	<Out extends PE24_Volatile_Dataset, In extends D1_Digital_Object> 
	extends IsRelatedTo<Out, In> {

}
