package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E29_Design_or_Procedure;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * P33 used specific technique (was used by)
 * Domain: E7 Activity
 * Range: E29 Design or Procedure
 * Subproperty of: E7 Activity. P16 used specific object (was used for): 
 * E70 Thing
 * Quantification: many to many (0,n:0,n)
 * 
 * Scope note: This property identifies a specific instance of E29 Design or 
 * Procedure in order to carry out an instance of E7 Activity or parts of it.
 * The property differs from P32 used general technique (was technique of) in 
 * that P33 refers to an instance of E29 Design or Procedure, which is a 
 * concrete information object in its own right rather than simply being a term 
 * or a method known by tradition.
 * Typical examples would include intervention plans for conservation or the 
 * construction plans of a building
 * 
 * Examples:
 * - Ornamentation of silver cup 232 (E11) used specific technique 
 * ‘Instructions for golden chase work by A N Other’ (E29)
 *  * - Rebuilding of Reichstag (E11) used specific technique Architectural plans 
 * by Foster and Partners (E29)
 * 
 * In First Order Logic:
 * P33(x,y) ⊃ E7(x)
 * P33(x,y) ⊃ E29(y)
 * P33(x,y) ⊃ P16(x,y)
 */
public interface P33_used_specific_technique
	<Out extends E7_Activity, In extends E29_Design_or_Procedure>
		extends IsRelatedTo<Out, In>  {

}
