package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D14_Software;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE6_Software_Hosting_Service;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP7_hosts_software_object
	<Out extends PE6_Software_Hosting_Service, In extends D14_Software>
		extends PP6_hosts_digital_object<Out,In> {

}
