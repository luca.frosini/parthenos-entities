package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E74_Group;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE34_Team extends E74_Group {

}
