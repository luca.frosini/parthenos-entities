package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E55_Type;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Domain: E7 Activity
 * Range: E55 Type
 * 
 * Quantification: many to many (0,n:0,n)
 * 
 * Scope note:
 * This property describes an intentional relationship between an E7 Activity 
 * and some general goal or purpose.
 * 
 * This may involve activities intended as preparation for some type of activity or event. 
 * P21had general purpose (was purpose of) differs from P20 had specific 
 * purpose (was purpose of) in that no occurrence of an event is implied as the purpose.
 * 
 * Examples:
 * Van Eyck’s pigment grinding (E7) had general purpose painting (E55)
 * The setting of trap 2742 on May 17 th 1874 (E7) had general purpose Catching Moose (E55)
 * (Activity type
 * 
 * In First Order Logic:
 * P21(x,y) ⊃ E7(x)
 * P21(x,y) ⊃ E55(y)
 * 
 */
public interface P21_had_general_purpose
	<Out extends E7_Activity, In extends E55_Type> extends IsRelatedTo<Out, In> {

}
