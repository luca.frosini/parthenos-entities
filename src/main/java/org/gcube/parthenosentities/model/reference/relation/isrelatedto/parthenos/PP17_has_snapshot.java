package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE19_Persistent_Digital_Object;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE20_Volatile_Digital_Object;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P130_shows_features_of;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP17_has_snapshot
	<Out extends PE20_Volatile_Digital_Object, In extends PE19_Persistent_Digital_Object>
		extends P130_shows_features_of<Out, In> {

}
