package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * P9 consists of (forms part of)
 * Domain: E4 Period
 * Range: E4 Period
 * Subproperty of: E92 Spacetime Volume. P132 spatiotemporally overlaps 
 * with.:E92 Spacetime Volume
 * Quantification: one to many, (0,n:0,1)
 * 
 * Scope note: This property associates an instance of E4 Period with another 
 * instance of E4 Period that is defined by a subset of the phenomena that 
 * define the former. Therefore the spacetime volume of the latter must fall
 * within the spacetime volume of the former.
 * 
 * This property is transitive.
 * 
 * Examples:
 * Cretan Bronze Age (E4) consists of Middle Minoan (E4)
 * 
 * In First Order Logic:
 * P9(x,y) ⊃ E4(x)
 * P9(x,y) ⊃ E4(y)
 * P9(x,y) ⊃ P10(y,x)
 * 
 */
public interface P9_consists_of
	<Out extends Resource, In extends Resource>
		extends IsRelatedTo<Out, In>  {

}
