package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE17_Curated_Data_EService extends
		PE12_Data_Curating_Service, PE15_Data_EService {

	//public static final String NAME = "PE17_Curated_Data_E-Service";
	
}
