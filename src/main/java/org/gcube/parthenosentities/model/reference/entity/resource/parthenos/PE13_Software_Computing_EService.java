package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE13_Software_Computing_EService extends PE8_EService,
		PE6_Software_Hosting_Service {
	
	//public static final String NAME = "PE13_Software_Computing_E-Service";
	
}
