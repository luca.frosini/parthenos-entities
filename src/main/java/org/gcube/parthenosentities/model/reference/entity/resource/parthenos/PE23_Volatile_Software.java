package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D14_Software;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE23_Volatile_Software extends D14_Software,
		PE20_Volatile_Digital_Object {

}
