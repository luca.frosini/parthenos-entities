package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E70_Thing;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE32_Curated_Thing extends E70_Thing {

}
