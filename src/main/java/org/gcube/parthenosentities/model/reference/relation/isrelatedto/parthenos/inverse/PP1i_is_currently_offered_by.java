package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;

import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.inverse.P9i_forms_part_of;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP1i_is_currently_offered_by extends P9i_forms_part_of {

}
