package org.gcube.parthenosentities.model.reference.relation.consistsof.parthenos;

import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.parthenosentities.model.reference.entity.facet.parthenos.PE29_Access_Point;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP50_accessible_at
	<Out extends D1_Digital_Object, In extends PE29_Access_Point>
		extends ConsistsOf<Out, In> {

}
