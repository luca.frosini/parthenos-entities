package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;

import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.inverse.P129i_is_subject_of;



/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP39i_has_metadata extends P129i_is_subject_of {

}
