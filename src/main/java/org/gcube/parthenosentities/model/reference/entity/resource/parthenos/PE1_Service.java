package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.parthenosentities.model.reference.entity.facet.parthenos.E30_Right;
import org.gcube.parthenosentities.model.reference.entity.facet.parthenos.PE_Info_Facet;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Service;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Attached facets are:
 *  - {@link IdentifierFacet} to map ID
 *  - {@link PE_Info_Facet} to map {title, description, competence,availability}  
 *  
 *  - {@link EventFacet} to map startTime
 *  - {@link EventFacet} to map endTime
 *
 *  - {@link E30_Right} to map rights and licenses
 *  
 *  
 *  Last confirmation : mappend to header.lastUpdatetime
 *  Date of Registration (w/Parthenos) : mappend to header.creationTime
 *  
 *  
 */
@Abstract
public interface PE1_Service extends Service, E7_Activity {

}
