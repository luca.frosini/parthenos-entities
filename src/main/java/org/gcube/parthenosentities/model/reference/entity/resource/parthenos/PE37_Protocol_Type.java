package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E55_Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE37_Protocol_Type extends E55_Type {

}
