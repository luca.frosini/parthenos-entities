package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E39_Actor;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;

/**
 * @author Luca Frosini (ISTI - CNR) 
 * 
 * P14 carried out by (performed) 
 * 
 * Domain: E7 Activity 
 * Range: E39 Actor 
 * Subproperty of: E5 Event. P11 had participant (participated in): E39 Actor 
 * Superproperty of: E8 Acquisition. P22 transferred title to (acquired title 
 *         through): E39 Actor E8 Acquisition. P23 transferred title from 
 *         (surrendered title through): E39 Actor E10 Transfer of Custody. P28 
 *         custody surrendered by (surrendered custody through): E39 Actor E10 
 *         Transfer of Custody. P29 custody received by (received custody 
 *         through): E39 Actor
 * Quantification: many to many, necessary (1,n:0,n) 
 * 
 * Scope note: This
 *         property describes the active participation of an E39 Actor in an E7
 *         Activity. It implies causal or legal responsibility. The P14.1 in the
 *         role of property of the property allows the nature of an Actor’s
 *         participation to be specified. 
 * 
 * Examples: 
 *  the painting of the Sistine Chapel (E7) carried out by Michaelangelo 
 * Buonaroti (E21) in the role of master craftsman (E55)
 * 
 * In First Order Logic:
 * P14 (x,y) ⊃ E7(x) 
 * P14 (x,y)⊃ E39(y) 
 * P14 (x,y) ⊃ P11(x,y) 
 * P14(x,y,z) ⊃ [P14(x,y) ∧ E55(z)] 
 * 
 * Properties: P14.1 in the role of: E55 Type
 */
public interface P14_carried_out_by<Out extends E7_Activity, In extends E39_Actor>
		extends IsRelatedTo<Out, In> {

}
