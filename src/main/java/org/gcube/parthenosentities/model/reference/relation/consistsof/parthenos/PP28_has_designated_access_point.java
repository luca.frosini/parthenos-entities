package org.gcube.parthenosentities.model.reference.relation.consistsof.parthenos;

import org.gcube.parthenosentities.model.reference.entity.facet.parthenos.PE29_Access_Point;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE8_EService;
import org.gcube.parthenosentities.model.reference.relation.consistsof.cidoc.P1_is_identified_by;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Links an instance of a PE8 E-Service to the web address at which the 
 * e-service can be accessed.
 */
public interface PP28_has_designated_access_point
	<Out extends PE8_EService, In extends PE29_Access_Point>
		extends P1_is_identified_by<Out, In> {

}
