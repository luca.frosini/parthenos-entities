package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE35_Project;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P9_consists_of;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP43_supported_project_activity
	<Out extends PE35_Project, In extends E7_Activity>
	extends P9_consists_of<Out, In>{

}
