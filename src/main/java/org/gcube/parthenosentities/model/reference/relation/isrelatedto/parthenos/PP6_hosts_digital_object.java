package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE5_Digital_Hosting_Service;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP6_hosts_digital_object
	<Out extends PE5_Digital_Hosting_Service, In extends D1_Digital_Object> 
		extends PP4_hosts_object<Out,In> {

}
