package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE22_Persistent_Dataset extends PE18_Dataset,
		PE19_Persistent_Digital_Object {

}
