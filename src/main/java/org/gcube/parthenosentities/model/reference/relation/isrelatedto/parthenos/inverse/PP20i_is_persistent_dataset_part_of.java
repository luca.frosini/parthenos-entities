package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;



/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP20i_is_persistent_dataset_part_of extends
		PP16i_is_persistent_digital_object_part_of {

}
