package org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc;

import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * Domain: E90 Symbolic Object
 * Range: E90 Symbolic Object
 * 
 * Quantification: many to many (0,n:0,n)
 * 
 * Scope note: This property associates an instance of E90 Symbolic Object 
 * with a part of it that is by itself an instance of E90 Symbolic Object, 
 * such as fragments of texts or clippings from an image.
 * 
 * This property is transitive.
 * 
 * Examples:
 * 
 * This Scope note P106 (E33) is composed of fragments of texts (E33)
 * ‘recognizable’ P106 (E90) is composed of ‘ecognizabl’ (E90)
 * 
 * In First Order Logic:
 * P106(x,y) ⊃ E90(x)
 * P106(x,y) ⊃ E90(y)
 * 
 */
public interface P106_is_composed_of<Out extends Resource, In extends Resource>
		extends IsRelatedTo<Out, In> {

}
