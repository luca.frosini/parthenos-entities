package org.gcube.parthenosentities.model.reference.entity.facet.parthenos;

import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;

/**
 * @author Luca Frosini (ISTI - CNR)
 * @author Alessia Bardi (ISTI - CNR)
 */
public interface PE_Info_Facet extends PE_Basic_Info_Facet {

	public static final String DESCRIPTION = "This facet is expected to capture minimal metadata for PE1_Service";
	public static final String VERSION = "1.0.0";
	
	@ISProperty
	public ValueSchema getCompetence();
	
	public void setCompetence(ValueSchema competence);
	
	@ISProperty
	public ValueSchema getAvailability();
	
	public void setAvailability(ValueSchema availability);
	
}
