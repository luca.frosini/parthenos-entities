package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;

import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.inverse.P16i_was_used_for;


/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP40i_is_deprecated_by extends P16i_was_used_for {

}
