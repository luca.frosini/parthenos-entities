package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E70_Thing;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE2_Hosting_Service;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P16_used_specific_object;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP4_hosts_object
	<Out extends PE2_Hosting_Service, In extends E70_Thing> 
		extends P16_used_specific_object<Out,In> {

}
