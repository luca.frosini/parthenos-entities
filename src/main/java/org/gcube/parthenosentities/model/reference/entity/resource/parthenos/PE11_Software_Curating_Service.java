package org.gcube.parthenosentities.model.reference.entity.resource.parthenos;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PE11_Software_Curating_Service extends
		PE10_Digital_Curating_Service {

}
