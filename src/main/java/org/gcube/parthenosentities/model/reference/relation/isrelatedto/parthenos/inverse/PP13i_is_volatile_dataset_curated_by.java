package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;




/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP13i_is_volatile_dataset_curated_by extends
		PP11i_is_volatile_digital_object_curated_by {

}
