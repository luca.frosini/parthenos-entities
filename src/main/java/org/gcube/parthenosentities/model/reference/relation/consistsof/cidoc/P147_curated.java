package org.gcube.parthenosentities.model.reference.relation.consistsof.cidoc;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE32_Curated_Thing;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE3_Curating_Service;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.PP32_curates;

public interface P147_curated<Out extends PE3_Curating_Service, In extends PE32_Curated_Thing>
		extends PP32_curates<Out,In> {
	
}
