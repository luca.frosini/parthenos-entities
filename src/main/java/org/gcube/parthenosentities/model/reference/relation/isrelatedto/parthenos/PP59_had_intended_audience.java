package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.E7_Activity;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE44_Audience_Type;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P21_had_general_purpose;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP59_had_intended_audience
	<Out extends E7_Activity, In extends PE44_Audience_Type> 
		extends P21_had_general_purpose<Out, In>  {

}
