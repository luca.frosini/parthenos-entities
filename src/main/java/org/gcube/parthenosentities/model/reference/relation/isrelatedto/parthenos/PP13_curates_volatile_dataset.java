package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos;

import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE12_Data_Curating_Service;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE24_Volatile_Dataset;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP13_curates_volatile_dataset
	<Out extends PE12_Data_Curating_Service, In extends PE24_Volatile_Dataset> 
		extends PP11_curates_volatile_digital_object<Out,In> {

}
