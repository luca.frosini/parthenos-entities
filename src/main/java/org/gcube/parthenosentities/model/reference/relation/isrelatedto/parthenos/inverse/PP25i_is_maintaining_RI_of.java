package org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.inverse;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface PP25i_is_maintaining_RI_of extends PP44i_is_maintaining_team_of {

}
