package org.gcube.parthenosentities.model;

import java.util.ArrayList;
import java.util.List;

import org.gcube.informationsystem.utils.discovery.RegistrationProvider;
import org.gcube.parthenosentities.model.reference.entity.facet.cidoc.E51_Contact_Point;
import org.gcube.parthenosentities.model.reference.entity.facet.parthenos.E30_Right;
import org.gcube.parthenosentities.model.reference.entity.resource.cidoc.D1_Digital_Object;
import org.gcube.parthenosentities.model.reference.entity.resource.parthenos.PE1_Service;
import org.gcube.parthenosentities.model.reference.relation.consistsof.cidoc.P1_is_identified_by;
import org.gcube.parthenosentities.model.reference.relation.consistsof.parthenos.PP28_has_designated_access_point;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.cidoc.P106_is_composed_of;
import org.gcube.parthenosentities.model.reference.relation.isrelatedto.parthenos.PP1_currently_offers;

public class PARTHENOSEntitiesRegistrationProvider implements RegistrationProvider {

	@Override
	public List<Package> getPackagesToRegister() {
		List<Package> packages = new ArrayList<>();
		packages.add(E51_Contact_Point.class.getPackage());
		packages.add(E30_Right.class.getPackage());
		packages.add(D1_Digital_Object.class.getPackage());
		packages.add(PE1_Service.class.getPackage());
		packages.add(P1_is_identified_by.class.getPackage());
		packages.add(PP28_has_designated_access_point.class.getPackage());
		packages.add(P106_is_composed_of.class.getPackage());
		packages.add(PP1_currently_offers.class.getPackage());
		return packages;
	}
	
}
